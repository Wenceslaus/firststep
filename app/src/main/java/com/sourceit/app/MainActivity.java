package com.sourceit.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_INT = "MainActivity.extra_int";

    EditText login;
    EditText pass;
    TextView loginError;
    TextView passError;
    Button action;

    private int value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login = findViewById(R.id.login);
        pass = findViewById(R.id.pass);
        action = findViewById(R.id.action);

        if (savedInstanceState != null) {
            value = savedInstanceState.getInt(EXTRA_INT);
            login.setText(String.valueOf(value));
        }

        loginError = findViewById(R.id.login_error);
        passError = findViewById(R.id.pass_error);

        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login.getText().toString().isEmpty() && pass.getText().toString().isEmpty()) {
                    AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Error")
                            .setMessage("Data invalid")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create();
                    dialog.show();
                } else {
                    Toast.makeText(v.getContext(), "Will be implemented in future", Toast.LENGTH_SHORT).show();
                }
            }
        });

        action.setOnClickListener(v -> {
            if (!checkAndShowErrors()) {
                Toast.makeText(v.getContext(), "Will be implemented in future", Toast.LENGTH_SHORT).show();
            }
        });

        login.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                //
            }
        });
    }

    private boolean checkAndShowErrors() {
        boolean hasError = false;
        if (login.getText().toString().isEmpty()) {
            hasError = true;
            loginError.setVisibility(View.VISIBLE);
        } else {
            loginError.setVisibility(View.INVISIBLE);
        }

        if (pass.getText().toString().isEmpty()) {
            hasError = true;
            passError.setVisibility(View.VISIBLE);
        } else {
            passError.setVisibility(View.GONE);
        }
        return hasError;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_INT, value);
    }
}
